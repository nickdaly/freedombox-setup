#!/bin/sh

enable_serial_console() {
    # By default, spawn a console on the serial port
    echo "Adding a getty on the serial port"
    echo "T0:12345:respawn:/sbin/getty -L ttyS0 115200 vt100" >> /etc/inittab
}

dreamplug_flash() {
    # allow flash-kernel to work without valid /proc contents
    # ** this doesn't *really* work, since there are too many checks
    #    that fail in an emulated environment!  We'll have to do it by
    #    hand below anyway...
    export FK_MACHINE="Globalscale Technologies Dreamplug"

    # Installing flash-kernel here, as installing it using debootstrap
    # causes all kernel related postinst scripts to stop working when
    # installed in a chroot.
    chroot $rootdir apt-get install -y flash-kernel
}

dreamplug_repack_kernel() {
# process installed kernel to create uImage, uInitrd, dtb
# using flash-kernel would be a good approach, except it fails in the
# cross build environment due to too many environment checks...
#FK_MACHINE="Globalscale Technologies Dreamplug" flash-kernel
#  so, let's do it manually...

# flash-kernel's hook-functions provided to mkinitramfs have the
# unfortunate side-effect of creating /conf/param.conf in the initrd
# when run from our emulated chroot environment, which means our root=
# on the kernel command line is completely ignored!  repack the initrd
# to remove this evil...

    echo "info: repacking dreamplug kernel and initrd"

    kernelVersion=$(ls /usr/lib/*/kirkwood-dreamplug.dtb | head -1 | cut -d/ -f4)
    version=$(echo $kernelVersion | sed 's/linux-image-\(.*\)/\1/')
    initRd=initrd.img-$version
    vmlinuz=vmlinuz-$version

    mkdir /tmp/initrd-repack

    (cd /tmp/initrd-repack ; \
	zcat /boot/$initRd | cpio -i ; \
	rm -f conf/param.conf ; \
	find . | cpio --quiet -o -H newc | \
	gzip -9 > /boot/$initRd )

    rm -rf /tmp/initrd-repack

    (cd /boot ; \
	cp /usr/lib/$kernelVersion/kirkwood-dreamplug.dtb dtb ; \
	cat $vmlinuz dtb >> temp-kernel ; \
	mkimage -A arm -O linux -T kernel -n "Debian kernel ${version}" \
	-C none -a 0x8000 -e 0x8000 -d temp-kernel uImage ; \
	rm -f temp-kernel ; \
	mkimage -A arm -O linux -T ramdisk -C gzip -a 0x0 -e 0x0 \
	-n "Debian ramdisk ${version}" \
	-d $initRd uInitrd )
}

# Install binary blob and kernel needed to boot on the Raspberry Pi.
raspberry_setup_boot() {
    # Packages used by rpi-update to make Raspberry Pi bootable
    apt-get install -y git-core binutils ca-certificates wget kmod

    wget https://raw.github.com/Hexxeh/rpi-update/master/rpi-update \
	-O /usr/bin/rpi-update
    chmod a+x /usr/bin/rpi-update
    mkdir -p /lib/modules
    touch /boot/start.elf
    SKIP_BACKUP=1 rpi-update | tee /root/rpi-update.log

}

# Install patched u-boot and kernel needed to boot on the Beaglebone.
beaglebone_setup_boot() {
    cp /usr/lib/u-boot/am335x_boneblack/MLO /boot/MLO
    cp /usr/lib/u-boot/am335x_boneblack/u-boot.img /boot/u-boot.img

    # uEnv.txt for Beaglebone
    # based on https://github.com/beagleboard/image-builder/blob/master/target/boot/beagleboard.org.txt
    cat >> /boot/uEnv.txt <<"EOF"
mmcroot=/dev/mmcblk0p2 ro
mmcrootfstype=ext4 rootwait fixrtc

console=ttyO0,115200n8

kernel_file=zImage

loadaddr=0x82000000
fdtaddr=0x88000000

fdt_high=0xffffffff

loadkernel=load mmc ${mmcdev}:${mmcpart} ${loadaddr} ${kernel_file}
loadfdt=load mmc ${mmcdev}:${mmcpart} ${fdtaddr} /dtbs/${fdtfile}

loadfiles=run loadkernel; run loadfdt
mmcargs=setenv bootargs console=tty0 console=${console} root=${mmcroot} rootfstype=${mmcrootfstype}

uenvcmd=run loadfiles; run mmcargs; bootz ${loadaddr} - ${fdtaddr}
EOF

# We want to fetch all packages from Debian, not download random binary blobs
# from somewhere on the net.
#    wget -O /tmp/latest-kernel https://rcn-ee.net/deb/"$SUITE"-"$ARCHITECTURE"/LATEST-omap-psp
#    kernel_version=`cat /tmp/latest-kernel | awk -F/ '/TESTING/ { print $6 }' | cut -c 2-`
#
#    wget -O /tmp/index.html https://rcn-ee.net/deb/"$SUITE"-"$ARCHITECTURE"/v"$kernel_version"/
#    kernel_filename=`cat /tmp/index.html | awk -F'[<=>"\""]*' '/linux-image/ { print $3 }'`
#    dtbs_filename=`cat /tmp/index.html | awk -F'[<=>"\""]*' '/dtbs/ { print $3 }'`
#
#    wget -O /tmp/linux-image.deb https://rcn-ee.net/deb/"$SUITE"-"$ARCHITECTURE"/v"$kernel_version"/"$kernel_filename"
#    dpkg -i /tmp/linux-image.deb
#    cp /boot/vmlinuz-"$kernel_version" /boot/zImage
#
#    wget -O /tmp/dtbs.tar.gz https://rcn-ee.net/deb/"$SUITE"-"$ARCHITECTURE"/v"$kernel_version"/"$dtbs_filename"
#    mkdir -p /boot/dtbs
#    tar xfvo /tmp/dtbs.tar.gz -C /boot/dtbs
}

tmp_on_tmpfs() {
    if grep -q /tmp /etc/fstab ; then
	:
    else
	echo "info: adding /tmp/ as tmpfs to /etc/fstab"
	cat >> /etc/fstab <<EOF
tmpfs     /tmp          tmpfs   rw,nosuid,nodev 0       0
EOF
    fi
}

tmp_on_tmpfs

case "$MACHINE" in
    dreamplug|guruplug)
	dreamplug_flash
	dreamplug_repack_kernel
	enable_serial_console
	;;
    raspberry)
	raspberry_setup_boot
	;;
    beaglebone)
	beaglebone_setup_boot
	;;
esac

echo "info: rewriting /etc/hosts, leave host specific info to libnss-myhostname."
cat > /etc/hosts <<EOF
# Only generic entries in /etc/hosts, host specific information is
# provided by libnss-myhostname.
127.0.0.1       localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF
